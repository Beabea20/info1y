/*

Scrivere un programma che chiede all'utente di inserire dele stringhe (senza spazi e di lunghezza al max 100).
Il programma termina non appena l'utente inserisce la stringa "stop", stampando il numero di stringhe inserite dall'utente
di lunghezza l (dove l è a sua volta inserito dall'utente all'inzio del programma)

*/

#include <stdio.h>
#include <string.h>

#define N 100

int leggiNumeroInteroPositivo();

int main()
{

    int lunghezza, n = 0;
    lunghezza = leggiNumeroInteroPositivo();

    char stringa[N], stop[] = "stop";
    printf("Inserisci una stringa senza spazi (stop per terminare):\n");
    do {
        scanf("%s%*c", stringa);
        if((strlen(stringa) == lunghezza) && (strcmp(stringa,stop) != 0)) //la parte dopo && serve per ignorare la stringa stop dal conteggio di n se la lunghezza inserita è = 4
            n++;
    } while(strcmp(stringa,stop) != 0);

    printf("Il numero di stringhe di lunghezza %d è %d\n", lunghezza, n);


    return 0;
}

int leggiNumeroInteroPositivo() {

    float num_orig;
    int num;

    do {
        printf("Insersci la lunghezza: ");
        scanf("%f%*c", &num_orig);
        num = num_orig;
    } while(num < 1 || num != num_orig  || num > N); //cicla finchè non inseriamo un numero positivo e interno, e minore di N poichè la lunghezza massima della stringa è N

    return num;
}
