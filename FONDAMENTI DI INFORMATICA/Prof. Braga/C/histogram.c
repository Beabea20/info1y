//

#include <stdio.h>

#define MAX 5

int main() {

    int array [MAX]; // = {19,3,15,7,11,9,13,5,17,1};
    
    int i,j;
    
    printf("Insert values: ");      //mi faccio dare i valori in input
    
    for(i=0;i<MAX;i++){
        
        scanf("%d", &array[i]);
        
    }
    
    printf("\n%s%13s%17s\n\n","Element","Value","Histogram");           //stampa i nomi delle colonne
    
    for(i=0;i<MAX;i++){
        
        printf("%4d%15d%s",i+1,array[i], "         ");          //stampa l'indice e il valore
        
        for (j=1; j<=array[i]; j++) {

                printf("*");            //stampa l'istogramma
            
        }
        
        printf("\n\n");
    
    }


    return 0;

}
