#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

typedef char TipoElemento;

typedef struct EL {
  TipoElemento info;
  struct EL * prox;
} ElemLista;

typedef ElemLista * ListaDiElem;

int verificapresenzaric(ListaDiElem lista, TipoElemento info);
void visualizzalistaric(ListaDiElem lista);
int verificapresenzaric(ListaDiElem lista, TipoElemento info);
void distruggilistaric(ListaDiElem lista2);
ListaDiElem insintesta(ListaDiElem lista, TipoElemento elem);
ListaDiElem invertilista(ListaDiElem lista);
ListaDiElem cancellaunoric(ListaDiElem lista, TipoElemento elem);
ListaDiElem insincodaric(ListaDiElem lista, TipoElemento elem);
ListaDiElem elimina_primo(ListaDiElem lista);

int main(){
	
	FILE * file;
	char c;
	ListaDiElem lista=NULL;
	if((file=fopen("testo.txt", "r"))!=NULL){
		for( ; !feof(file) ; ){
			c=fgetc(file);
			if(c=='{' || c=='[' || c=='('){
				lista=insintesta(lista, c);
			}
			else if(c=='}'){
				if(lista->info==c-('}'-'{')){
					elimina_primo(lista);
				}
				else{
					printf("Errore di sintassi delle parentesi");
					return 0;
				}
			}
			else if(c==']'){
				if(lista->info==c-(']'-'[')){
					elimina_primo(lista);
				}
				else{
					printf("Errore di sintassi delle parentesi");
					return 0;
				}
			}
			else if(c==')'){
				if(lista->info==c-(')'-'(')){
					elimina_primo(lista);
				}
				else{
					printf("Errore di sintassi delle parentesi");
					return 0;
				}
			}
		}
		if(lista==NULL){
			printf("La sintassi delle parentesi e' corretta");
		}
		else{
			printf("La sintassi delle parentesi e' errata (paentesi rimaste aperte)");
		}
		fclose(file);
	}
	else{
		printf("Errore nella lettura del file \"testo.txt\"");
	}
	
	return 0;
}

ListaDiElem elimina_primo(ListaDiElem lista){
	ListaDiElem temp=NULL;
	temp=lista->prox;
	free(lista);
	lista=temp;
	return lista;
}

ListaDiElem insincodaric(ListaDiElem lista, TipoElemento elem){
	ListaDiElem temp;
	if(lista!=NULL){
		lista->prox=insincodaric(lista->prox, elem);
		return lista;
	}
	else{
		lista= (ListaDiElem) malloc(sizeof(ElemLista));
		lista->info=elem;
		lista->prox=NULL;
		return lista;
	}
}

ListaDiElem cancellaunoric(ListaDiElem lista, TipoElemento elem){
	ListaDiElem temp;
	if(lista->info==elem){
		temp=lista;
		lista=lista->prox;
		free(temp);
		return lista;
	}
	else if(lista->prox!=NULL){
		lista->prox=cancellaunoric(lista->prox, elem);
		return lista;
	}
	else if(lista->prox==NULL){
		return lista;
	}
}

ListaDiElem invertilista(ListaDiElem lista){
	ListaDiElem temp=NULL, lista2;
	lista2=lista;
	for(; lista!=NULL; ){
		temp=insintesta(temp, lista->info);
		lista=lista->prox;
	}
	distruggilistaric(lista2);
	return temp;
}

ListaDiElem insintesta(ListaDiElem lista, TipoElemento elem){
	ListaDiElem temp;
	temp= (ListaDiElem) malloc(sizeof(ElemLista));
	temp->info=elem;
	temp->prox=lista;
	lista=temp;
}


void distruggilistaric(ListaDiElem lista){
	if(!listavuota(lista)){
		distruggilistaric(lista->prox);
		free(lista);
		lista=NULL;
	}
}

int verificapresenzaric(ListaDiElem lista, TipoElemento info){
	if(lista->info==info){
		return 1;
	}
	else{
		if(lista->prox!=NULL){
			return verificapresenzaric(lista->prox, info);
		}
		else{
			return 0;
		}
	}
}

void visualizzalistaric(ListaDiElem lista){
	if(!listavuota(lista)){
		printf("%d", lista->info);
		if(!listavuota(lista->prox)){
			printf("-->");
		}
		lista=lista->prox;
		visualizzalistaric(lista);
	}
	else{
		printf("--|\n");
	}
}

int listavuota(ListaDiElem lista){
	if(lista==NULL){
		return 1;
	}
	else{
		return 0;
	}
}
