#include <stdio.h>
#include <string.h>
#include <math.h>

int main(){
	
	char s1[100] = "", s2[100] = "", s3[100] = "";
	int p=0, i=0;
	
	printf("--Allacciamo le parole, basic version (1/2)--\n");
	printf("Inserisci due parole allacciabili (caSA,SAlame): ");
	scanf("%s", s1);
	scanf("%s", s2);
	
	for(i=1, p=-1; i<=strlen(s1)-1 && i<strlen(s2); i++){
		p++;
		if(s1[i]!=s2[p]){
			p=-1;
		}
	}
	strcpy(s3,s1);
	s3[i-1-p]='\0';
	if(p==-1 || p==strlen(s2)-1 || (s1[0]==s2[0] && p==strlen(s1)-2)){
		printf("Le parole (%s,%s) non sono allacciabili", s1, s2);
	}
	else{
		printf("Le parole (%s,%s) sono allacciabili==> %s", s1, s2, strcat(s3,s2));
	}
	
	return 0;
}
